# README

## Challenge 2

A simple server responding to the routes `/foo` and `/bar`. The server is
implemented using the NodeJS Express Framework in [`index.js`](index.js).
The routes are implemented in [`routes.js`](routes.js) using
[Express Router](https://expressjs.com/en/guide/routing.html).

The routes contain GET methods returning `{"response":"Hello"}` on route `/foo`
and `{"response":"World"}` on route `/bar`, e.g. pointing a browser to
`http://localhost:3000/test/foo` shows `{"response":"Hello"}`.

- The base url of the routes can be set via the environment variable `BASE_URL`.
  `BASE_URL` defaults to `''`
- The port can be set via the environment variable `PORT`. `PORT` defaults to
  `3333`.

## Install

Requires [node](https://nodejs.org/en/)

```
$: git clone https://gitlab.com/janpb/fzjch2.git app
$: cd app
$: npm install
```

## Start

To start the server, run the following command in the app directory. It will
start a server on the `localhost` listening on port 3141 and use the base URL
`/test`. The path to the base URL will be printed to STDOUT.

```
$: cd app
$: PORT=3141 BASE_URL="/test" npm run express_challenge_two.js
Serving routes on
  http://localhost:3333/foo
  http://localhost:3333/bar
```
## Test

The routes can be tested using `wget`:

```
for route in foo bar; do  printf "%s\n" $(wget -qO -  http://localhost:3141/test/${route}); done
```

## Docker image

The server can be run as a Docker container.

### Building the image

```
$: cd app
$: docker build -t cc2 .
```
This will build an image using the default `PORT 3333` and `BASE_URL=''`.

### Run container

Run the server in a container and map the host port 3000 to the container
port 3000.
```
$: docker run -p 3000:3000 cc2
```

#### Test
```
for route in foo bar; do  printf "%s\n" $(wget -qO -  http://localhost:3000/${route}); done
```

### Adjust `PORT` and `BASE_URL`

PORT and BASE_URL can be set during build or during run.

- During build: `docker build  -t cc2 --build-arg port=4000 --build-arg baseurl=/conbio .`

    - Run: `docker run  -p 3000:4000  cc2`
    - Test: `for route in foo bar; do  printf "%s\n" $(wget -qO -  http://localhost:3000/conbio/${route}); done`

- During run: `docker run --env PORT=5000 -e BASE_URL=/biocon  -p 3000:5000 cc2`
    - Test: `for route in foo bar; do  printf "%s\n" $(wget -qO -  http://localhost:3000/biocon/${route}); done`

