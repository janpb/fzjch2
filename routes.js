// Author: Jan P Buchmann <jpb@members.fsf.org>
// Licence: GPLv3
// Description: Router file to handle requests on routes foo and bar using
// express.Router. Responses are in JSON.

var express = require('express')
var router = express.Router()

// Sends response for route /baseurl/foo
router.get('/foo', function (req, res)
{
  return res.json({'response':'Hello'})
})

// Sends response for route /baseurl/bar
router.get('/bar', function (req, res)
{
  return res.json({'response':'World'})
})

module.exports = router
