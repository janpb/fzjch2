// Author: Jan P Buchmann <jpb@members.fsf.org>
// Licence: GPLv3
// Description: Script to start server with given parameters.
// Usage: PORT=2000 BASE_URL="/conbio" node index.js


// Loads Express framework and imports routes. Enforce default values.
"use strict";
const routes = require('./routes')
const express = require('express')
const defport = 3333
const defbase = ''

// Reads port and base URL from the corresponding environment variables. Falls
// back to default values if none are given.
var port = process.env.PORT || defport
var baseurl = process.env.BASE_URL || defbase

// Starts the server
const app = express().use(baseurl, routes)
app.listen(port, () => console.log(`Serving routes on
  http://localhost:${port}${baseurl}/foo
  http://localhost:${port}${baseurl}/bar`))
