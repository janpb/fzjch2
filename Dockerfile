#
# Author Jan P Buchmann <jpb@members.fsf.org>
# Dockerfile for Express Challenge Two - Web Servers

# Set ARGS to persist across build stages
ARG builddir=/builds
ARG appdir=app


FROM alpine:3.11 as base
ARG builddir
ARG appdir
ARG src="https://gitlab.com/janpb/fzjch2.git"
RUN apk add --no-cache git &&  mkdir -p ${builddir} &&  \
    git -C ${builddir} clone ${src} ${appdir}


FROM alpine:3.11
# Set docker user
ARG user="wharfie"
# Setup system
RUN apk add --no-cache nodejs npm                   && \
    adduser --gecos "" --disabled-password ${user}

# Set up npm framework as user
ARG builddir
ARG appdir
USER ${user}
WORKDIR /home/${user}
RUN mkdir -p /home/${user}/${appdir}
COPY --from=base --chown=${user}:${user} ${builddir}/${appdir}/*.js ${appdir}/
COPY --from=base --chown=${user}:${user} ${builddir}/${appdir}/*.json ${appdir}/
WORKDIR /home/${user}/${appdir}
RUN npm install

# Set PORT and BASEURL for server via arguments or use defaults.
ARG port
ARG ${port:-3333}
ARG baseurl
ARG ${baseurl:+baseurl}

# Set environment variables for server
ENV PORT=${port}
ENV BASE_URL=${baseurl}

# Open port at build
EXPOSE ${PORT}

# Start server. Adjusting the baseurl or port during run can be done as follows:
# run docker run -e PORT=<appport> -e BASE_URL=<baseurl> -p <hostport>:<appport>
# point browser to http://localhost:<hostport>/<baseurl>/{foo,bar}

CMD ["npm", "start"]

LABEL autho="Jan P Buchmann, <jpb@members.fsf.org>"
LABEL usage="default: \
              docker run  -p 3141:3000 chlg2 => http://localhost:3141 \
            adjust:   \
              docker run  PORT=4000 -p 2718:4000 chlg2 => http://localhost:2718"
